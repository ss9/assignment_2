package net.therap.controller;

import net.therap.domain.Person;
import net.therap.domain.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * @author shadman
 * @since 11/7/17
 */
public interface Validator {

    static List<ValidationError> validate(Person p) {
        return new ArrayList<ValidationError>();
    }

    static void print(List<ValidationError> list) {

    }
}
