package net.therap.controller;

import net.therap.domain.Person;
import net.therap.domain.ValidationError;

import java.util.List;

/**
 * @author shadman
 * @since 11/7/17
 */
public class Main {

    public static void main(String[] args){
        Person p = new Person("Abcde Fghijk", 5);
        List<ValidationError> errors = AnnotatedValidator.validate(p);
        AnnotatedValidator.print(errors);
    }
}
