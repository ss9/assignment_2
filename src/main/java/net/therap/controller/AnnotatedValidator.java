package net.therap.controller;

import net.therap.domain.Person;
import net.therap.domain.Size;
import net.therap.domain.ValidationError;
import net.therap.view.Printer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shadman
 * @since 11/7/17
 */
public class AnnotatedValidator implements Validator {

    public static List<ValidationError> validate(Person p) {
        List<ValidationError> list = new ArrayList<ValidationError>();
        Field[] fields = Person.class.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getAnnotations();
            for (Annotation a : annotations) {
                if (a instanceof Size) {
                    try {
                        String errorMessage = null;
                        Integer min = ((Size) a).min();
                        Integer max = ((Size) a).max();
                        String message = ((Size) a).message();
                        message = message.replace("{min}", min.toString());
                        message = message.replace("{max}", max.toString());
                        Class classOfField = field.get(p).getClass();
                        String simpleClassOfField = classOfField.getSimpleName();
                        if (simpleClassOfField.equals("String")) {
                            String s = (String) field.get(p);
                            if (s.length() < min || s.length() > max) {
                                errorMessage = message;
                            }
                        } else if (simpleClassOfField.equals("Integer")) {
                            Integer i = (Integer) field.get(p);
                            if (i < min || i > max) {
                                errorMessage = message;
                            }
                        }
                        if (errorMessage instanceof String) {
                            list.add(new ValidationError(field, errorMessage));
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return list;
    }

    public static void print(List<ValidationError> list) {
        Printer.print(list);
    }
}
