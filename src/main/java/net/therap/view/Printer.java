package net.therap.view;

import net.therap.domain.ValidationError;

import java.util.List;

/**
 * @author shadman
 * @since 11/7/17
 */
public class Printer {

    public static void print(List<ValidationError> list) {
        for (ValidationError ve : list) {
            System.out.println(ve);
        }
    }
}
