package net.therap.domain;

/**
 * @author shadman
 * @since 11/7/17
 */
public class Person {

    @Size(max = 10)
    private String name;

    @Size(min = 18, message = "Age cannot be less than {min}")
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
