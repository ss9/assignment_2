package net.therap.domain;

import java.lang.reflect.Field;

/**
 * @author shadman
 * @since 11/7/17
 */
public class ValidationError {

    private Field field;
    private String errorMessage;

    public ValidationError(Field field, String errorMessage) {
        this.field = field;
        this.errorMessage = errorMessage;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String toString() {
        return field.getName() + "(" + field.getType().getSimpleName() + "): " + this.errorMessage;
    }
}
