package net.therap.domain;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author shadman
 * @since 11/7/17
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface Size {

    int min() default 1;

    int max() default 100;
    
    String message() default "Length must be {min} - {max}";
}
